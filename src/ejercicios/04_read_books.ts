// Crea una función isBookRead que reciba una lista de libros y un título y devuelva si se ha leído
// o no dicho libro. Un libro es un objeto con title como string y isRead como booleano.
// En caso de no existir el libro devolver false;

class book {
    constructor(
        public title: string, 
        public isRead: boolean,
    ) {}
}

const books = [
    new book('Harry Potter y la piedra filosofal', true),
    new book('Canción de hielo y fuego', false),
    new book('Devastación', true),
];

const isBookRead = (books: book[], titleToSearch: string) => {
    const [readBook] = books.filter(book => book.title === titleToSearch);
    return readBook ? readBook.isRead : false;
};

console.log(isBookRead(books, "Devastación")); // true
console.log(isBookRead(books, "Canción de hielo y fuego")); // false
console.log(isBookRead(books, "Los Pilares de la Tierra")); // false