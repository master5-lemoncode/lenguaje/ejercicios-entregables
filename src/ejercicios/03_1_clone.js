// Implementa una función clone que, a par r de un objeto de entrada source devuelva un nuevo
// objeto con las propiedades de source :
 
const clone = (source = {}) => Object.assign({}, source);

const objOriginal = {
    nombre: 'pepe',
    color: 'azul',
    bebida: 'cerveza',
};

const objClonado = clone(objOriginal);

console.log(objOriginal, objClonado, objOriginal === objClonado, objOriginal.nombre === objClonado.nombre);