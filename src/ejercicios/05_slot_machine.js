var SlotMachine = /** @class */ (function () {
    function SlotMachine() {
        this.countPlay = 0;
        this.ruleta1 = false;
        this.ruleta2 = false;
        this.ruleta3 = false;
    }
    SlotMachine.prototype.play = function () {
        this.runPlay();
        this.isWinnerPlay() ?
            this.win() : this.lose();
    };
    SlotMachine.prototype.runPlay = function () {
        this.countPlay++;
        this.ruleta1 = this.runRuleta();
        this.ruleta2 = this.runRuleta();
        this.ruleta3 = this.runRuleta();
    };
    SlotMachine.prototype.runRuleta = function () {
        return Math.random() >= 0.5;
    };
    SlotMachine.prototype.isWinnerPlay = function () {
        return this.ruleta1 && this.ruleta2 && this.ruleta3;
    };
    SlotMachine.prototype.win = function () {
        console.log("Congratulations!!!. You won " + this.countPlay + " coins!!");
        this.countPlay = 0;
    };
    SlotMachine.prototype.lose = function () {
        console.log("Good luck next time!!");
    };
    return SlotMachine;
}());
var machine1 = new SlotMachine();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
