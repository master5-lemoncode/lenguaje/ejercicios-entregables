class SlotMachine {
    private countPlay: number = 0;
    private ruleta1: boolean = false;
    private ruleta2: boolean = false;
    private ruleta3: boolean = false;

    constructor() {}

    play(){
        this.runPlay();
        this.isWinnerPlay() ?
            this.win() : this.lose();
    }

    private runPlay() {
        this.countPlay++;
        this.ruleta1 = this.runRuleta();
        this.ruleta2 = this.runRuleta();
        this.ruleta3 = this.runRuleta();
    }
    private runRuleta(): boolean {
        return Math.random() >= 0.5;
    }
    private isWinnerPlay() : boolean {
        return this.ruleta1 && this.ruleta2 && this.ruleta3;
    }
    private win(): void {
        console.log(`Congratulations!!!. You won ${this.countPlay} coins!!`);
        this.countPlay = 0;
    }
    private lose(): void {
        console.log(`Good luck next time!!`);
    }
}

const machine1 = new SlotMachine();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();
machine1.play();