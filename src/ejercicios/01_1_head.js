
// Implementa una función head (inmutable), tal que, dado un array 
// como entrada extraiga y devuelva su primer elemento.

const head = ([first] = []) => first;

const sabores1 = ['fresa', 'chocolate', 'vainilla'];

console.log(head(sabores1));