
// Implementa una función init (inmutable), tal que, dado un array como entrada devuelva 
// todos los elementos menos el último. Utiliza los métodos que ofrece Array.prototype.

const last = (sabores = []) => sabores.pop();

const sabores4 = ['fresa', 'chocolate', 'vainilla'];

console.log(last(sabores4));