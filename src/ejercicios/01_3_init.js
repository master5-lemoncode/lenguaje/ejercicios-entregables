
// Implementa una función init (inmutable), tal que, dado un array como entrada devuelva 
// todos los elementos menos el último. Utiliza los métodos que ofrece Array.prototype.

const init = (sabores = []) => sabores.slice(0, sabores.length-1);

const sabores3 = ['fresa', 'chocolate', 'vainilla'];

console.log(init(sabores3));