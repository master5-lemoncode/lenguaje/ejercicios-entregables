
// Implementa una función tail (inmutable), tal que, dado un array como entrada devuelta todos 
// menos el primer elemento. Utiliza rest operator.

const tail = ([, ...restoSabores] = []) => restoSabores;

const sabores2 = ['fresa', 'chocolate', 'vainilla'];

console.log(tail(sabores2));