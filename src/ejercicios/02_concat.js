
// Implementa una función concat (inmutable) tal que, dados 2 arrays como entrada, 
// devuelva la concatenación de ambos. Utiliza rest / spread operators.

const concat = (a = [], b = []) => [...a, ...b];

const saboresA = ['fresa', 'chocolate', 'vainilla'];
const saboresB = ['cafe', 'nata', 'avellana'];

console.log(concat(saboresA, saboresB));

// Implementa una versión del ejercicio anterior donde se acepten múltiples arrays de entrada (más de 2).

const saboresC = ['yogurt', 'menta'];
const saboresD = ['limón', 'strachiatella'];

const concatNarrays = (...args) => 
    args.reduce((acumulador, valor) => [...acumulador, ...valor]);

console.log(concatNarrays(saboresA, saboresB, saboresC, saboresD));
